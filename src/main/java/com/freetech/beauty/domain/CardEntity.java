package com.freetech.beauty.domain;

import jakarta.persistence.*;

@Entity
@Table(schema="public", name="cards")
public class CardEntity {
    @Id
    @SequenceGenerator(name = "CARD_GENERATOR", sequenceName = "public.seq_cards", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CARD_GENERATOR")
    @Column(name = "id", nullable = false)
    private Long id;

    @JoinColumn(name = "person_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private PersonEntity personEntity;

    @Column(name = "message", nullable = false)
    private String message;

    @Column(name = "thumbail", nullable = false)
    private String thumbail;

    @Column(name = "thumbailback", nullable = false)
    private String thumbailBack;

    @Column(name = "audio", nullable = false)
    private String audio;

    @Column(name = "love", nullable = false)
    private Integer love;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonEntity getPerson() {
        return personEntity;
    }

    public void setPerson(PersonEntity personEntity) {
        this.personEntity = personEntity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getThumbail() {
        return thumbail;
    }

    public void setThumbail(String thumbail) {
        this.thumbail = thumbail;
    }

    public String getThumbailBack() {
        return thumbailBack;
    }

    public void setThumbailBack(String thumbailBack) {
        this.thumbailBack = thumbailBack;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public Integer getLove() {
        return love;
    }

    public void setLove(Integer love) {
        this.love = love;
    }
}
