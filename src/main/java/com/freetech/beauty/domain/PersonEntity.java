package com.freetech.beauty.domain;

import jakarta.persistence.*;

@Entity
@Table(schema="public", name="persons")
public class PersonEntity {
    @Id
    @SequenceGenerator(name = "PER_GENERATOR", sequenceName = "public.seq_persons", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PER_GENERATOR")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "lastname", nullable = false)
    private String lastname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
