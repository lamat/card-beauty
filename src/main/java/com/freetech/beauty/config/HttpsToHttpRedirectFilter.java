package com.freetech.beauty.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

public class HttpsToHttpRedirectFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if ("https".equals(request.getScheme())) {
            String redirectUrl = "http://" + request.getServerName() + request.getRequestURI();
            response.sendRedirect(redirectUrl);
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
