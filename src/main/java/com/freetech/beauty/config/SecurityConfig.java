package com.freetech.beauty.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.cors(cors -> {
            CorsConfiguration configuration = new CorsConfiguration();
            configuration.setAllowedOrigins(Arrays.asList("*"));
            configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "HEAD", "OPTIONS"));
            configuration.setAllowedHeaders(Arrays.asList("*"));
            cors.configurationSource(request -> configuration);

            UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
            source.registerCorsConfiguration("/**", configuration);

            cors.configurationSource(source::getCorsConfiguration);
        }).csrf(AbstractHttpConfigurer::disable);
        http.addFilterBefore(httpsToHttpRedirectFilter(), ChannelProcessingFilter.class);
        return http.build();
    }

    @Bean
    public HttpsToHttpRedirectFilter httpsToHttpRedirectFilter() {
        return new HttpsToHttpRedirectFilter();
    }
}
