package com.freetech.beauty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeautyCardApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeautyCardApplication.class, args);
	}

}
