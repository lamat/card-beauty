package com.freetech.beauty.repository;

import com.freetech.beauty.domain.CardEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CardRepository {

    private final BaseRepository baseRepository;

    public CardRepository(BaseRepository baseRepository) {
        this.baseRepository = baseRepository;
    }

    public CardEntity updateCard(Long id) {
        var cardEntity = this.baseRepository.getById(id, CardEntity.class);
        cardEntity.setLove(cardEntity.getLove() == 1 ? 0 : 1);
        return this.baseRepository.update(cardEntity);
    }

    public List<CardEntity> getCardsByPerson(Long personId) {
        return this.baseRepository.getByField("personEntity.id", personId, CardEntity.class);
    }
}
