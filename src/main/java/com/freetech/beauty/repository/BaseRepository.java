package com.freetech.beauty.repository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BaseRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public <K,T> T getById(K id, Class<T> clazz) {
        return entityManager.find(clazz, id);
    }

    @Transactional
    public <T> T update(T entity) {
        return entityManager.merge(entity);
    }

    public <T> List<T> getByField(String field, Object value, Class<T> clazz) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(field.replace(".", ""), value);

        StringBuilder sql = new StringBuilder();
        sql.append( "SELECT t FROM " )
                .append( clazz.getName() )
                .append( " t WHERE t.")
                .append( field )
                .append( " = :" )
                .append( field.replace(".","") );

        TypedQuery<T> query = createQuery(sql.toString(), parameters, clazz);
        return query.getResultList();
    }

    private <X> TypedQuery<X> createQuery(String jpql, Map<String, Object> parameters, Class<X> entityClass) {
        TypedQuery<X> query = entityManager.createQuery(jpql, entityClass);
        if (parameters != null) {
            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                query.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return query;
    }
}
