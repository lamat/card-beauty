package com.freetech.beauty.controller;

import com.freetech.beauty.entities.Card;
import com.freetech.beauty.service.CardService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "v1/cards")
public class CardController {
    private final CardService cardService;

    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<List<Card>> getCards(@PathVariable("id") Long id) {
        return new ResponseEntity<>(cardService.getCardsByPerson(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Card> updateCard(@PathVariable("id") Long id) {
        return new ResponseEntity<>(cardService.updateCard(id), HttpStatus.OK);
    }
}