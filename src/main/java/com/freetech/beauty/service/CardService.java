package com.freetech.beauty.service;

import com.freetech.beauty.domain.CardEntity;
import com.freetech.beauty.entities.Card;
import com.freetech.beauty.repository.CardRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardService {
    private final CardRepository cardRepository;

    public CardService(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    public List<Card> getCardsByPerson(Long personId) {
        return this.cardRepository.getCardsByPerson(personId).stream().map(x -> toCard(x))
                .collect(Collectors.toList());
    }

    public Card updateCard(Long id) {
        return toCard(this.cardRepository.updateCard(id));
    }

    private Card toCard(CardEntity cardEntity) {
        var card = new Card();
        card.setId(cardEntity.getId());
        card.setMessage(cardEntity.getMessage());
        card.setThumbail(cardEntity.getThumbail());
        card.setThumbailBack(cardEntity.getThumbailBack());
        card.setAudio(cardEntity.getAudio());
        card.setLove(cardEntity.getLove());
        return card;
    }
}
