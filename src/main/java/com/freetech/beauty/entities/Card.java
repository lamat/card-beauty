package com.freetech.beauty.entities;

public class Card {
    private Long id;
    private String message;
    private String thumbail;
    private String thumbailBack;
    private String audio;
    private Integer love;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getThumbail() {
        return thumbail;
    }

    public void setThumbail(String thumbail) {
        this.thumbail = thumbail;
    }

    public String getThumbailBack() {
        return thumbailBack;
    }

    public void setThumbailBack(String thumbailBack) {
        this.thumbailBack = thumbailBack;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public Integer getLove() {
        return love;
    }

    public void setLove(Integer love) {
        this.love = love;
    }
}
